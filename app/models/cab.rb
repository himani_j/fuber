class Cab < ActiveRecord::Base
  self.table_name="cabs"
  scope :available, -> { where(available: true)}
  scope :pink_only, -> { where(pink: true) }

  has_one :ride,
    :primary_key => :id,:foreign_key => :cab_id

  def reserve
    self.update_attribute(:available, false)
  end

  def un_reserve(dest_latitude, dest_longitude)
    self.update_attributes(available: true, latitude: dest_latitude, longitude: dest_longitude)
  end
end