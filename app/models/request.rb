class Request < ActiveRecord::Base
  self.table_name="requests"

  has_one :ride,
    :primary_key => :id,:foreign_key => :request_id

  include DistanceCalculator

  def is_fulfillable?
    if self.pink_only?
      not Cab.available.pink_only.empty?
    else
      not Cab.available.empty?
    end
  end

  def get_nearest_cab
    cabs = self.pink_only? ? Cab.available.pink_only : Cab.available
    min_distance = Float::INFINITY
    cab_id = nil
    cabs.each do |cab|
      distance = calculate_distance(self.source_latitude, self.source_longitude, cab.latitude, cab.longitude )
      if distance < min_distance
        min_distance = distance 
        cab_id = cab.id
      end
    end
    cab_id
  end
end