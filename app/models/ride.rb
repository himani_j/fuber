class Ride < ActiveRecord::Base
  self.table_name="rides"

  belongs_to :request,
    :primary_key => :id, :foreign_key => :request_id

  belongs_to :cab,
    :primary_key => :id, :foreign_key => :cab_id

  include DistanceCalculator
  include FareCalculator

  def start_ride(time)
    self.update_attribute(:start_time, time)
  end

  def complete_ride(dest_latitude, dest_longitude, end_time)
    distance_in_km =  calculate_distance(self.request.source_latitude, self.request.source_longitude, dest_latitude, dest_longitude)
    duration_in_minutes = ((end_time - self.start_time.to_datetime)*1.days)/60
    fare_in_dogecoins = calculate_fare(distance_in_km, duration_in_minutes, self.cab.pink)

    self.update_attributes(end_time: end_time, dest_latitude: dest_latitude, dest_longitude: dest_longitude, distance: distance_in_km, duration: duration_in_minutes, fare: fare_in_dogecoins)
    self.cab.un_reserve(dest_latitude, dest_longitude)
  end
end