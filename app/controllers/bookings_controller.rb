class BookingsController < ApplicationController
  include BookingsHelper

  attr_accessor :ride_id

  def book_ride
    request = Request.create(source_latitude: params["latitude"], source_longitude: params["longitude"], pink_only: params["pink_only"])
    @ride_id = request.is_fulfillable? ? make_booking(request) : nil
    render :json => { "ride_id" => ride_id}.to_json
  end

  def start_ride
    Ride.find(ride_id).start_ride(DateTime.now)
    render :json => { "status" => "started"}.to_json
  end

  def complete_ride
    Ride.find(ride_id).complete_ride(params["latitude"], params["longitude"], DateTime.now)
    render :json => { "status" => "completed"}.to_json
  end
end
