module BookingsHelper
  def make_booking(request)
    cab_id = request.get_nearest_cab
    Cab.find(cab_id).reserve
    ride = Ride.create(request_id: request.id, cab_id: cab_id)
    ride.id
  end
end
