module DistanceCalculator
  def calculate_distance(source_lat, source_long, dest_lat, dest_long)
    x_diff = source_lat.to_f - dest_lat.to_f
    y_diff = source_long.to_f - dest_long.to_f
    Math.sqrt((x_diff*x_diff) + (y_diff*y_diff))
  end
end