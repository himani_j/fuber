describe BookingsController, type: :controller do

  let(:params) do
    {
      "latitude"=>234.3, 
      "longitude"=>240.5, 
      "pink_only"=>true
    }
  end
  before { create(:cab, {id: 2, pink:  true})}

  describe "POST #book_ride" do
    it "returns a ride_id if cab is available, else returns nil" do
      post :book_ride, params
      expect(assigns(:ride_id)).to eq(1)
    end

    it "Creates a new request" do
      expect(post :book_ride, params).to change(Request,:count).by(1)
    end
  end


  describe "POST #start_ride" do
    before {post :book_ride, params}

    it "return json with status" do
      post :start_ride
      expect(response.body) == { "status" => "started"}.to_json
    end
  end

  describe "POST #complete_ride" do
    before {post :book_ride, params}
    before {post :start_ride}

    it "return json with status" do
      post :complete_ride, params
      expect(response.body) == { "status" => "completed"}.to_json
    end
  end
  end

