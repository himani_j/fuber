describe Ride, type: :model do
  context "with pink_only request " do
    before { create(:cab, {pink:  true})}
    before { create(:request, {pink_only: true})}

    subject { create(:ride)}
    let(:start_time) { DateTime.now }
    let(:end_time) { start_time+10.minutes}
    let(:dest_latitude) {145.34}
    let(:dest_longitude) {123.5}
    
    it "#start_ride" do
      subject.start_ride(start_time)
      expect(subject.start_time).not_to be_nil
    end

    it "#complete_ride" do
      subject.start_ride(start_time)
      subject.complete_ride(dest_latitude, dest_longitude, end_time)
      expect(subject.end_time).not_to be_nil
      expect(subject.distance).to eq 352.378233720529
      expect(subject.duration).to eq 10.0
      expect(subject.fare).to eq 719.756467441059
    end
  end
end