describe Cab, type: :model do
  before { create(:cab)}
  subject { Cab.first}
  let(:new_latitude) {145.34}
  let(:new_longitude) {123.5}

  it "reserves a cab and make it unavailable for other requests" do
    subject.reserve
    expect(subject.available).to eq false
  end

  it "updates lacation and makes cab available for other requests" do
    subject.un_reserve(new_latitude, new_longitude)
    expect(subject.available).to eq true
    expect(subject.latitude).to eq(145.34)
    expect(subject.longitude).to eq(123.5)
  end
end