describe Request, type: :model do
  context "with pink_only " do
    before { create(:cab)}
    before { create(:cab, {id: 2, pink:  true})}

    subject { create(:request, {pink_only: true})}
    
    it "returns fulfillablity " do
      expect(subject.is_fulfillable?).to eq(true)
    end

    it "returns id of nearest available cab " do
      expect(subject.get_nearest_cab).to eq(2)
    end
  end
end