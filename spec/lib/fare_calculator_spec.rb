describe FareCalculator do
  let(:wrapper){
    class MyModuleWrapper
      include FareCalculator
    end
    MyModuleWrapper.new
  }

  let(:distance_in_km) { 352.37823372052935 }
  let(:duration_in_min) { 10.0 }
  let(:pink) { true }

  it "#calculate_distance" do
    expect(wrapper.calculate_fare(distance_in_km, duration_in_min, pink)).to eq(719.7564674410587)
  end

end