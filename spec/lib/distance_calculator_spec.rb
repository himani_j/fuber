describe DistanceCalculator do
  let(:wrapper){
    class MyModuleWrapper
      include DistanceCalculator
    end
    MyModuleWrapper.new
  }

  let(:source_lat) { 245.3 }
  let(:source_long) { 259.5 }
  let(:dest_lat) { 250.7 }
  let(:dest_long) { 260.9 }

  it "#calculate_distance" do
    expect(wrapper.calculate_distance(source_lat, source_long, dest_lat, dest_long)).to eq(5.578530272392514)
  end

end