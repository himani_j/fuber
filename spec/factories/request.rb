FactoryGirl.define do
  factory :request do
    id 1
    source_latitude 434.7
    source_longitude 324.6
    pink_only false
  end
end