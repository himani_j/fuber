FactoryGirl.define do
  factory :cab do
    id 1
    pink false
    latitude 323.34
    longitude 329.6
    available true
  end
end