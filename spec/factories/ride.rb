FactoryGirl.define do
  factory :ride do
    id 1
    request_id 1 
    cab_id 1
    start_time nil
    end_time nil
    duration nil
    dest_latitude nil
    dest_longitude nil
    distance nil
    fare nil
  end
end