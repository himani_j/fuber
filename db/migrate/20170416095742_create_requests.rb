class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.decimal :source_latitude
      t.decimal :source_longitude
      t.string :pink_only
    end
  end
end
