class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.integer :request_id
      t.integer :cab_id
      t.datetime :start_time
      t.datetime :end_time
      t.decimal :duration
      t.decimal :dest_latitude
      t.decimal :dest_longitude
      t.decimal :distance
      t.decimal :fare
    end
  end
end
