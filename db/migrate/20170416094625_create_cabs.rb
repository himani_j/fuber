class CreateCabs < ActiveRecord::Migration
  def change
    create_table :cabs do |t|
      t.boolean :pink
      t.decimal :latitude
      t.decimal :longitude
      t.boolean :available
    end
  end
end
