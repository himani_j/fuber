# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Cab.create([
  {pink: true, latitude: 323.34, longitude: 329.6, available: true},
  {pink: true, latitude: 323.1, longitude: 324.5, available: true},
  {pink: false, latitude: 343.1, longitude: 394.89, available: true}
  ])
